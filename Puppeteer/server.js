//Imports

const express = require('express');
const cors = require('cors');
const app = express();
app.use(cors());
const cron = require('node-cron');
const salesandqc = require('./sales.js')
const fs = require('fs');

//Constants
const port = process.env.PORT || 5001;
let ts = Date.now();
let date_ob = new Date(ts);
let dow = date_ob.getDay();

//Cron jobs
cron.schedule('0 0 22 * * *', async function getSalesCron(dow){
  await salesandqc.getSales(dow);
  await salesandqc.moveDownload()
  console.log('running SalesAndQC task every day!');
});

cron.schedule('0 30 21 * * *', async function removeSales(){
  await deleteSales();
  console.log("Deleted Sales and QC");
});

//Endpoints
app.get('/download/salesandqc', (req, res) => {
  const file = `./data/January 2023 Sales & QC Log.xlsx`;
  res.download(file);
});

app.get('/pupp/dailySales', async (req, res) => {
  await salesandqc.getSales(dow);
  await salesandqc.moveDownload()
  const file = `./data/January 2023 Sales & QC Log.xlsx`;
  //res.download(file);
});

app.get('/pupp/dailySales/delete', async (req, res) => {
  await 
  deleteSales();
  console.log("Deleted Sales and QC");
  res.send({data: 'Hopefully deleting!'});
});

//Functions
async function deleteSales(){
  fs.unlinkSync(`./data/January 2023 Sales & QC Log.xlsx`), function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Deleted Sales and QC");
    res.send({data: 'Hopefully deleting!'});
    }
  }
  }


app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});