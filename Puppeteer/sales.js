const puppeteer = require("puppeteer");
const fs = require('fs');

let ts = Date.now();
let date_ob = new Date(ts);
let dow = date_ob.getDay();

async function getSales(dow) {
    if (dow != 1) {


        const browser = await puppeteer.launch({
            executeablePath: '/usr/bin/chrome',
            headless: false,defaultViewport: null, args: ["--disable-gpu",
                "--disable-dev-shm-usage",
                "--disable-setuid-sandbox",
                "--no-sandbox",]
        });
        //`--start-maximized`
        const page = await browser.newPage();
        await page.goto('https://decroixintl-my.sharepoint.com/personal/adill_decroixintl_com/_layouts/15/onedrive.aspx?FolderCTID=0x01200044A0C9535FFEE34A9857240097601E25&id=%2Fpersonal%2Fadill%5Fdecroixintl%5Fcom%2FDocuments%2FSales%20and%20QC%2FDaily%20Logs%2F01%20January%202023');
        await page.screenshot({ path: "testing.png" });
        await page.waitForSelector('input[name=loginfmt]');
        await page.$eval('input[name=loginfmt]', el => el.value = 'mbreden@decroixintl.com');
        await page.waitForSelector("#idSIButton9")
        await page.click("#idSIButton9");
        await page.waitForTimeout(3000);
        await page.waitForSelector('input[name=passwd]');
        await page.$eval('input[name=passwd]', el => el.value = 'Wabbawinner123');
        await page.waitForTimeout(3000);
        await page.click(
            "#idSIButton9"
        );
        //await page.waitForTimeout(2000);
        await page.waitForSelector("#idBtn_Back")
        await page.click("#idBtn_Back");
        //await page.waitForTimeout(3000);
        await page.waitForSelector('div[title="January 2023 Sales & QC Log.xlsx"]');
        await page.click('div[title="January 2023 Sales & QC Log.xlsx"]');
        await page.waitForTimeout(3000)
        //await page.waitForSelector('Button[name="Download"]')
        //await page.click('Button[name="Download"]');
        await page.waitForSelector('[aria-label="More"] [class="ms-Button-flexContainer flexContainer-63"]');
        await page.click('[aria-label="More"] [class="ms-Button-flexContainer flexContainer-63"]');
        await page.waitForSelector('[data-icon-name="download"]');
        await page.click('[data-icon-name="download"]');
        await page.waitForTimeout(3000);

        await browser.close();
    }
};

function moveDownload() {
    var oldPath = '../../../../Downloads/January 2023 Sales & QC Log.xlsx'
    //var oldPath = 'C:\\Users\\mbred\\Downloads\\January 2023 Sales & QC Log.xlsx';
    var newPath = './data/January 2023 Sales & QC Log.xlsx';
    //var newPath = 'C:\\Users\\mbred\\Desktop\\GFS\\Work\\InternalSite\\internalDSI\\InternalDSI\\InternalDSI\\Puppeteer\\data';

    fs.renameSync(oldPath, newPath, function (err) {
        if (err) throw err;
        console.log('Renamed Sales and QC')
    });
    //     fs.unlinkSync(oldPath, function (err) {
    //         if (err) {
    //           console.log(err);
    //         } else {
    //           console.log("Deleted Sales and QC");
    // }
    // });
}

module.exports = {
    getSales,
    moveDownload
};
