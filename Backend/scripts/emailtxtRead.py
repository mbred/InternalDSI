from datetime import date
import os
today = date.today()
today = today.replace(day=today.day - 1)
date = today.strftime("%m%d")
year = today.year
os.chdir("./data")
email = 'email_' + date + "_" + str(year) + '.txt'
emailLoc = 'emails/' + email

with open(emailLoc, 'r') as f:
    contents = f.read()
    print(contents)
