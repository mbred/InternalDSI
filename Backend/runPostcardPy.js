const { spawn } = require('child_process');


async function printDailyPostcard() {
    return new Promise((resolve, reject) => {
      let data = '';
      var pythonExe = '/usr/bin/python3.9'
      const python = spawn(pythonExe, ['./scripts/emailtxtRead.py']);
  
      python.stdout.on('data', function(chunk) {
        data += chunk.toString();
      });
  
      python.on('close', (code) => {
        if (code !== 0) {
          reject(new Error(`Python process exited with code ${code}`));
        } else {
          console.log(data)
          resolve(data);
        }
      });
    });
  }

  async function runDailyPostcard() {
    return new Promise((resolve, reject) => {
      let data = '';
      var pythonExe = '/usr/bin/python3.9'
      const python = spawn(pythonExe, ['./scripts/SalesPostCodeSummary.py']);
  
      python.stdout.on('data', function(chunk) {
        data += chunk.toString();
      });
  
      python.on('close', (code) => {
        if (code !== 0) {
          reject(new Error(`Python process exited with code ${code}`));
        } else {
          //data = data.replace(/\r\n \r\n/g, '<br>');
          //data = data.replace(/\r\n/g, '<br>');
          //data = data.replace(/\r\n\r\n/g, '<br>');
          //data = data.replace(/\n/g, '&#10;').replace(/\r/g, '&#13;'); 
          console.log(data)
          resolve(data);
        }
      });
    });
  }
module.exports = {
  printDailyPostcard,
  runDailyPostcard
};
