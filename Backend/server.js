//Imports

const express = require('express');
const cors = require('cors');
const cron = require('node-cron');
const app = express();
const fs = require("fs");
const qs = require("qs");
//require('dotenv').config()
app.use(cors());
const axios = require('axios');
const dailyPostcard = require('./runPostcardPy');
//const {AuthenticationContext} = require("msal");

//Constants
const port = process.env.PORT || 5000;

//Graph API
const APP_ID = 'f0e91ed0-023f-45fe-8513-8c2e35488e27';
const APP_SECRET = 'Og~8Q~g2LFRznt8mcB1uHlYhAWADpEdwYix3WbZQ';
const TOKEN_ENDPOINT = 'https://login.microsoftonline.com/fa4b4f31-0c7d-4632-a6ba-f4a18a73fd19/oauth2/v2.0/token';
const MS_GRAPH_SCOPE = 'https://graph.microsoft.com/.default';
const postData = {
  client_id: APP_ID,
  scope: MS_GRAPH_SCOPE,
  client_secret: APP_SECRET,
  grant_type: 'client-credentials'
};

//Date
let ts = Date.now();
let date_ob = new Date(ts);
let date = date_ob.getDate();
let month = date_ob.getMonth();
let year = date_ob.getFullYear();


//Functions

//Function to get the Access_Token to utilize the API
async function getToken(APP_ID, APP_SECRET, TOKEN_ENDPOINT) {
  axios.defaults.headers.post['Content-Type'] =
    'application/x-www-form-urlencoded';

  let token;
  console.log(TOKEN_ENDPOINT);
  await axios
    .post(TOKEN_ENDPOINT, {
      client_id: APP_ID,
      client_secret: APP_SECRET,
      scope: 'https://graph.microsoft.com/.default',
      grant_type: 'client_credentials'
    })
    .then(response => {
      token = response.data.access_token;
      console.log(response.data);
      console.log(`getToken: ${token}`);
    })
    .catch(error => {
      console.log("We are failing")
      console.log(error);
    });

  return token;
}
//Function to call the graph api and get the Sales & QC excel file 
async function getSalesAndQC(APP_ID, APP_SECRET, TOKEN_ENDPOINT) {
  const url = 'https://graph.microsoft.com/v1.0/users/ccf02ce2-8054-48d0-9b32-932f2cbe3ee5/drives/b!zG6JUSZf9EWMXD1ijjQPt3o9PXwmoYFJtGmOQ212sHNxsAGNQw4URJo2e2g8pPTQ/items/01JNQEPRLYH6Y3Q3HJA5F2MA37VDBIBA3L?select=id,@microsoft.graph.downloadUrl'
  //const url = 'https://graph.microsoft.com/v1.0/me'
  const token = await getToken(APP_ID, APP_SECRET, TOKEN_ENDPOINT);
  console.log(`getSalesAndQC: ${token}`);
  let downloadUrl;

  const options = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  };
  //console.log(options);


  await axios.get(url, options)
    .then(response => {
      console.log(response.data);
      downloadUrl = response.data['@microsoft.graph.downloadUrl'];
      console.log(downloadUrl);
    })
    .catch(error => {
      console.log(error);
      //console.log('This is the problem')
    });
  var dest = './data/January 2023 Sales & QC Log.xlsx'
  var file = fs.createWriteStream(dest);


  await axios.get(downloadUrl, {
    responseType: 'stream'
  })
    .then(response => {
      response.data.pipe(file);
      file.on('finish', function () {
        file.close();
      })
      //console.log(response.data);
    })
    .catch(error => {
      console.log(error);
      //console.log('Failed Printing')
    });
}

async function deleteSales(){
  fs.unlinkSync(`./data/January 2023 Sales & QC Log.xlsx`), function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Deleted Sales and QC");
    res.send({data: 'Hopefully deleting!'});
    }
  }
  }

//API endpoints
app.get('/api/endpoint', (req, res) => {
  res.send({ data: 'Hello from the server!' });
});

app.get('/api/dailyPost', async (req, res) => {
  const dataToSend = await dailyPostcard.printDailyPostcard();
  res.send({ data: dataToSend });
  // var path ='C:/Users/mbred/Desktop/GFS/Work/InternalSite/internalDSI/InternalDSI/InternalDSI/Backend/data/emails/email_0109_2023.pdf';
  // res.sendFile(path);
  // var filename = "email_0109_2023.pdf";

  // filename = encodeURIComponent(filename);
  // res.setHeader('Content-disposition', 'inline; filename="' + filename + '"');
  // res.setHeader('Content-type', 'application/pdf');

  // stream.pipe(res);
});

app.get('/api/runDailyPost', async (req, res) => {
  await getSalesAndQC(APP_ID, APP_SECRET, TOKEN_ENDPOINT);
  await dailyPostcard.runDailyPostcard();
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});


//CRON JOBS in UTC 
// 1:00PM UTC -> 8:00AM EST
cron.schedule('0 0 12 * * *', async function getSalesAndQCDaily() {
  console.log('Cron Job: Getting Sales & QC files!');
  await getSalesAndQC(APP_ID, APP_SECRET, TOKEN_ENDPOINT);
  console.log('Cron Job: Sales and QC GOT');
  console.log('Cron Job: Running script on files')
  await dailyPostcard.runDailyPostcard();
  console.log('Cron Job: Files recieved!');
});
//12:57PM UTC -> 7:57AM EST
cron.schedule('0 57 11 * * *', async function deleteSalesDaily() {
  console.log('Cron Job: Deleting Sales & QC file');
  await deleteSales();
  console.log('Cron Jobn: Finished Deletes Sales & QC')
});